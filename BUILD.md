# Building from sources

## Requirements
- First you need to install some system dependencies in order to build Karapulse. The easiest way is to install the packages listed in
[the recipe of the Docker image used by CI](docker/Dockerfile#L5), just execute the `apt-get install` command on your system if you
are using Debian or Ubuntu.
You can also use this image directly for building if you're into Docker. It's available in the [project registry](https://gitlab.freedesktop.org/gdesmott/karapulse/container_registry).
- You may also have to install a few extra packages:
```
sudo apt install -y nodejs build-essential
```
- Then you'll have to [install meson](https://mesonbuild.com/Getting-meson.html):
```
pip3 install --user meson

```
- [Install Rust](https://www.rust-lang.org/learn/get-started)
```
curl https://sh.rustup.rs -sSf | sh
```
- Get the source code
```
git clone https://gitlab.freedesktop.org/gdesmott/karapulse.git
cd karapulse
```
- Karapulse uses [Rocket](https://rocket.rs/) which requires Rust nightly so let's enable it for this repo
```
rustup override set nightly
```

## Building
```
meson build -D cdg-plugin=true -D prefix=$PWD/install
ninja -C build install
```
- You can now launch `./install/bin/karapulse` and `./install/bin/karapulse-manage-db`

// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use serde::Serialize;
use std::collections::{HashMap, VecDeque};
use std::path::PathBuf;

use crate::db::Song;

#[derive(Debug, Serialize, Clone)]
pub struct QueueItem {
    #[serde(skip_serializing)]
    pub path: PathBuf,
    pub user: String,
    pub song: Option<Song>,
    #[serde(skip_serializing)]
    pub history_id: Option<i64>,
}

impl QueueItem {
    fn new(path: &PathBuf, user: &str, song: Option<&Song>, history_id: Option<i64>) -> Self {
        let path = path.clone();
        let user = user.to_string();
        let song = song.cloned();
        Self {
            path,
            user,
            song,
            history_id,
        }
    }
}

#[derive(Debug)]
pub struct Queue {
    songs: VecDeque<QueueItem>,
    last_pop: Option<String>, // owner of the last item poped from the queue
}

impl Queue {
    pub fn new() -> Queue {
        Queue {
            songs: VecDeque::new(),
            last_pop: None,
        }
    }

    #[allow(dead_code)]
    fn len(&self) -> usize {
        self.songs.len()
    }

    fn find_place(&self, user: &str) -> usize {
        /* Iterate the queue from the front to the tail. Take the place of
         * the second element owned by someone having more than one element
         * since the last element owned by @user. */
        let mut counts: HashMap<&str, u32> = HashMap::new();

        /* Count the last item popep out of the queue */
        if let Some(u) = &self.last_pop {
            counts.insert(&u, 1);
        }

        for (i, elt) in self.songs.iter().enumerate() {
            let owner = &elt.user;

            if owner == user {
                // current element has been added by @user, reset counters
                counts = HashMap::new();
                continue;
            }

            let count = match counts.get_mut::<str>(owner) {
                Some(count) => {
                    *count += 1;
                    *count
                }
                None => {
                    counts.insert(owner, 1);
                    1
                }
            };

            if count > 1 {
                return i;
            }
        }

        self.songs.len()
    }

    pub fn add(
        &mut self,
        path: &PathBuf,
        user: &str,
        song: Option<&Song>,
        history_id: Option<i64>,
    ) {
        let song = QueueItem::new(path, user, song, history_id);

        let idx = self.find_place(&user);
        self.songs.insert(idx, song);
    }

    pub fn next(&mut self) -> Option<QueueItem> {
        let item = self.songs.pop_front();

        if let Some(ref i) = item {
            self.last_pop = Some(i.user.clone())
        }

        item
    }

    pub fn snapshot(&self) -> Vec<QueueItem> {
        let mut v = Vec::new();
        self.songs.iter().for_each(|s| v.push(s.clone()));
        v
    }

    #[allow(dead_code)]
    pub fn iter(&self) -> std::collections::vec_deque::Iter<QueueItem> {
        self.songs.iter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::Path;

    #[test]
    fn add() {
        let mut q = Queue::new();
        assert_eq!(q.len(), 0);

        let p = PathBuf::from("/first");
        q.add(&p, "a", None, None);
        assert_eq!(q.len(), 1);

        let p = PathBuf::from("/second");
        q.add(&p, "b", None, None);
        assert_eq!(q.len(), 2);

        let snapshot = q.snapshot();
        assert_eq!(snapshot.len(), 2);

        let s = q.next().unwrap();
        assert_eq!(q.len(), 1);
        assert_eq!(s.path, Path::new("/first"));

        let s = q.next().unwrap();
        assert_eq!(q.len(), 0);
        assert_eq!(s.path, Path::new("/second"));

        let s = q.next();
        assert!(s.is_none());
    }

    fn check_order(check: &[&str], q: &Queue) {
        let tmp: Vec<&str> = q.iter().map(|x| x.path.to_str().unwrap()).collect();
        assert_eq!(tmp, check);
    }

    #[test]
    fn ordering_simple() {
        let mut q = Queue::new();
        q.add(&Path::new("/a1").to_path_buf(), "a", None, None);
        q.add(&Path::new("/a2").to_path_buf(), "a", None, None);
        q.add(&Path::new("/b1").to_path_buf(), "b", None, None);
        check_order(&["/a1", "/b1", "/a2"], &q);
    }

    #[test]
    fn ordering_multi() {
        let mut q = Queue::new();
        q.add(&Path::new("/a1").to_path_buf(), "a", None, None);
        q.add(&Path::new("/a2").to_path_buf(), "a", None, None);
        q.add(&Path::new("/b1").to_path_buf(), "b", None, None);
        q.add(&Path::new("/b2").to_path_buf(), "b", None, None);
        q.add(&Path::new("/a3").to_path_buf(), "a", None, None);
        q.add(&Path::new("/c1").to_path_buf(), "c", None, None);
        q.add(&Path::new("/a4").to_path_buf(), "a", None, None);
        check_order(&["/a1", "/b1", "/c1", "/a2", "/b2", "/a3", "/a4"], &q);
    }

    #[test]
    fn ordering_bug() {
        let mut q = Queue::new();
        q.add(&Path::new("/a1").to_path_buf(), "a", None, None);
        q.add(&Path::new("/b1").to_path_buf(), "b", None, None);
        q.add(&Path::new("/b2").to_path_buf(), "b", None, None);
        q.add(&Path::new("/a2").to_path_buf(), "a", None, None);
        check_order(&["/a1", "/b1", "/a2", "/b2"], &q);
    }

    #[test]
    fn ordering_pop() {
        let mut q = Queue::new();
        q.add(&Path::new("/a1").to_path_buf(), "a", None, None);
        /* File is played right away */
        q.next();
        q.add(&Path::new("/a2").to_path_buf(), "a", None, None);
        q.add(&Path::new("/b1").to_path_buf(), "b", None, None);
        check_order(&["/b1", "/a2"], &q);
    }
}

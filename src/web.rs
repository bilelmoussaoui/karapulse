// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use failure::Error;
use glib::Sender;
use std::path::Path;
use std::sync::Mutex;
use std::time::{Duration, Instant};

use rocket::config::{Config, Environment};
use rocket::http::Method;
use rocket::{get, routes, Rocket, State};
use rocket_contrib::json::Json;
use rocket_contrib::serve::StaticFiles;
use rocket_cors::{AllowedHeaders, AllowedOrigins};
use std::path::PathBuf;
use std::sync::mpsc::channel;

use crate::config;
use crate::db::{Song, DB};
use crate::karapulse::{Message, Reply};
use crate::protocol;

// The minimum of time, in seconds, between two valid 'next' request.
// This is used to prevent next collisions when different users want to next the same song.
pub const NEXT_IGNORE_WINDOW: u64 = 3;

struct Web {
    tx: Mutex<Sender<Message>>,
    db: Mutex<DB>,
    last_next: Mutex<Option<Instant>>,
}

impl Web {
    fn new(tx: Sender<Message>, db: DB) -> Web {
        Web {
            tx: Mutex::new(tx),
            db: Mutex::new(db),
            last_next: Mutex::new(None),
        }
    }
}

#[get("/play")]
fn play(web: State<Web>) {
    let tx = web.tx.lock().unwrap();
    tx.send(Message::Play).unwrap();
}

#[get("/pause")]
fn pause(web: State<Web>) {
    let tx = web.tx.lock().unwrap();
    tx.send(Message::Pause).unwrap();
}

#[get("/next")]
fn next(web: State<Web>) {
    {
        let mut last = web.last_next.lock().unwrap();
        if let Some(last) = *last {
            if last.elapsed() < Duration::from_secs(NEXT_IGNORE_WINDOW) {
                debug!("Ignore consecutive 'next' requests");
                return;
            }
        }
        *last = Some(Instant::now());
    }
    let tx = web.tx.lock().unwrap();
    tx.send(Message::Next).unwrap();
}

#[get("/search/<fields>")]
fn search(web: State<Web>, fields: String) -> Result<Json<protocol::SearchResponse>, Error> {
    debug!("search: {}", fields);
    let songs = {
        let db = web.db.lock().unwrap();
        db.search(&fields)?
    };

    let result = protocol::SearchResponse::new(songs);
    Ok(Json(result))
}

#[get("/enqueue_db/<id>/<user>")]
fn enqueue_db(web: State<Web>, id: i32, user: String) -> Option<()> {
    debug!("enqueue: {} from {}", id, user);
    let song = {
        let db = web.db.lock().unwrap();
        db.find_song(id)
    };
    match song {
        Ok(song) => {
            let tx = web.tx.lock().unwrap();

            tx.send(Message::Enqueue {
                path: song.path.clone(),
                user,
                song,
            })
            .unwrap();
            Some(())
        }
        Err(..) => None,
    }
}

#[get("/status")]
fn status(web: State<Web>) -> Result<Json<protocol::StatusResponse>, Error> {
    let rx = {
        let tx = web.tx.lock().unwrap();
        let (reply_tx, rx) = channel();

        tx.send(Message::GetStatus { reply_tx }).unwrap();
        rx
    };
    let d = Duration::from_secs(1);
    let reply = rx.recv_timeout(d);

    if let Err(err) = reply {
        let msg = format!("Didn't receive status reply: {:?}", err);
        warn!("{}", msg);
        bail!(msg);
    }

    match reply.unwrap() {
        Reply::Status { status } => Ok(Json(status)),
    }
}

#[get("/history")]
fn history(web: State<Web>) -> Result<Json<protocol::HistoryResponse>, Error> {
    let history = {
        let db = web.db.lock().unwrap();
        db.list_history()?
    };

    let result = protocol::HistoryResponse::new(history);
    Ok(Json(result))
}

#[get("/history/<user>")]
fn history_for(web: State<Web>, user: String) -> Result<Json<protocol::HistoryResponse>, Error> {
    let history = {
        let db = web.db.lock().unwrap();
        db.history_for(&user)?
    };

    let result = protocol::HistoryResponse::new(history);
    Ok(Json(result))
}

#[get("/all")]
fn all(web: State<Web>) -> Result<Json<Vec<Song>>, Error> {
    debug!("all");
    let songs = {
        let db = web.db.lock().unwrap();
        db.list_songs()?
    };

    Ok(Json(songs))
}

fn rocket(path: &PathBuf, tx: Sender<Message>, db: DB) -> Rocket {
    let web = Web::new(tx, db);

    let allowed_origins = AllowedOrigins::all();

    let cors = rocket_cors::CorsOptions {
        allowed_origins,
        allowed_methods: vec![Method::Get].into_iter().map(From::from).collect(),
        allowed_headers: AllowedHeaders::some(&["Authorization", "Accept"]),
        allow_credentials: true,
        ..Default::default()
    }
    .to_cors()
    .unwrap();

    let config = Config::build(Environment::Production)
        .port(1848)
        .finalize()
        .expect("Failed to crate Rocket configuration");

    rocket::custom(config)
        .mount(
            "/api",
            routes![
                play,
                pause,
                next,
                search,
                enqueue_db,
                status,
                history,
                history_for,
                all,
            ],
        )
        .mount("/", StaticFiles::from(path))
        .manage(web)
        .attach(cors)
}

pub fn start_web(tx: Sender<Message>, db: DB) {
    let path = Path::new(config::PKGDATADIR)
        .join("web")
        .canonicalize()
        .expect("Failed building 'web' directory");

    rocket(&path, tx, db).launch();
}

#[cfg(test)]
mod test {
    use super::rocket;
    use crate::db::Song;
    use crate::db::DB;
    use crate::karapulse;
    use crate::karapulse::{Message, Reply};
    use crate::protocol;
    use crate::queue::Queue;

    use glib::MainContext;
    use rocket::http::Status;
    use rocket::local::Client;
    use serde_json::{json, Value};
    use std::cell::RefCell;
    use std::env;
    use std::path::Path;
    use std::path::PathBuf;
    use std::rc::Rc;
    use std::sync::Arc;
    use std::thread;

    struct Test {
        client: Arc<Client>,
        // Need to keep the context alive during the test duration
        context: MainContext,
        message: Rc<RefCell<Option<Message>>>,
    }

    impl Test {
        fn new(
            client: Arc<Client>,
            context: MainContext,
            message: Rc<RefCell<Option<Message>>>,
        ) -> Test {
            Self {
                client,
                context,
                message,
            }
        }

        fn received_message(&self) -> Option<Message> {
            while self.message.borrow().is_none() {
                self.context.iteration(true);
            }
            self.message.replace(None)
        }

        fn start_get_query<'a>(
            &self,
            query: &'static str,
        ) -> thread::JoinHandle<(Status, Option<String>)> {
            let client = self.client.clone();
            thread::spawn(move || {
                let mut response = client.get(query).dispatch();
                (response.status(), response.body_string())
            })
        }

        fn get_query<'a>(&self, query: &'static str) -> (Status, Option<String>) {
            let thread = self.start_get_query(query);
            thread.join().unwrap()
        }

        fn get_no_reply(&self, query: &'static str) {
            let (status, response) = self.get_query(query);
            assert_eq!(status, Status::Ok);
            assert_eq!(response, None);
        }

        fn get_json(&self, query: &'static str) -> Value {
            let (status, response) = self.get_query(query);
            assert_eq!(status, Status::Ok);
            serde_json::from_str(&response.unwrap()).unwrap()
        }
    }

    fn populate_db(db: &DB) {
        db.add_song(&PathBuf::from("file1.mp3"), "artist1", "title1", Some(10))
            .expect("Failed adding song");

        let songs = db.list_songs().expect("Failed listing songs");

        let song = &songs[0];
        db.add_history(&song, "Alice")
            .expect("Failed adding history");
    }

    fn setup() -> Test {
        let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let db = DB::new_memory().expect("Failed creating DB");

        populate_db(&db);

        let context = glib::MainContext::new();
        context.acquire();

        let message = Rc::new(RefCell::new(None));
        let message_clone = message.clone();
        rx.attach(Some(&context), move |msg| {
            let message = &message_clone;

            match msg {
                Message::GetStatus { ref reply_tx } => {
                    let mut queue = Queue::new();

                    let song1 = Song {
                        rowid: 1,
                        path: "file1.mp3".to_string(),
                        artist: "artist1".to_string(),
                        title: "title1".to_string(),
                        length: Some(10),
                    };
                    queue.add(&Path::new("/a1").to_path_buf(), "alice", Some(&song1), None);

                    let song2 = Song {
                        rowid: 2,
                        path: "file2.mp3".to_string(),
                        artist: "artist2".to_string(),
                        title: "title2".to_string(),
                        length: Some(15),
                    };
                    queue.add(&Path::new("/a2").to_path_buf(), "bob", Some(&song2), None);

                    let current_song = queue
                        .next()
                        .map_or(None, |s| Some(protocol::StatusResponseSong::new(s, None)));
                    let queue = queue
                        .snapshot()
                        .into_iter()
                        .map(|s| protocol::StatusResponseSong::new(s, Some(5)))
                        .collect();

                    let status = protocol::StatusResponse::new(
                        karapulse::State::Playing,
                        current_song,
                        Some(5),
                        queue,
                    );
                    reply_tx
                        .send(Reply::Status { status })
                        .expect("Failed sending status reply");
                }
                _ => {}
            };

            message.replace(Some(msg));
            glib::Continue(true)
        });

        // Can't server the actual web client as it may not be build
        let path = env::temp_dir();

        let client = Arc::new(Client::new(rocket(&path, tx, db)).expect("valid rocket instance"));
        Test::new(client, context, message)
    }

    #[test]
    fn play() {
        let test = setup();
        test.get_no_reply("/api/play");
        assert_eq!(test.received_message(), Some(Message::Play));
    }

    #[test]
    fn pause() {
        let test = setup();
        test.get_no_reply("/api/pause");
        assert_eq!(test.received_message(), Some(Message::Pause));
    }

    #[test]
    fn next() {
        let test = setup();
        test.get_no_reply("/api/next");
        assert_eq!(test.received_message(), Some(Message::Next));
    }

    #[test]
    fn search() {
        let test = setup();
        assert_eq!(test.get_json("/api/search/nope"), json!([]));
        assert_eq!(
            test.get_json("/api/search/artist1"),
            json!([ { "id": 1, "artist": "artist1", "title": "title1", "length": 10 } ]),
        );
    }

    #[test]
    fn enqueue_db() {
        let test = setup();

        test.get_no_reply("/api/enqueue_db/1/test");
        assert_eq!(
            test.received_message(),
            Some(Message::Enqueue {
                path: "file1.mp3".to_string(),
                user: "test".to_string(),
                song: Song {
                    rowid: 1,
                    path: "file1.mp3".to_string(),
                    artist: "artist1".to_string(),
                    title: "title1".to_string(),
                    length: Some(10),
                },
            })
        );

        let (status, _response) = test.get_query("/api/enqueue_db/100/test");
        assert_eq!(status.code, 404);
    }

    #[test]
    fn status() {
        let test = setup();
        let thread = test.start_get_query("/api/status");
        test.received_message();
        let (status, response) = thread.join().unwrap();
        assert_eq!(status, Status::Ok);
        let reply: Value = serde_json::from_str(&response.unwrap()).unwrap();
        assert_eq!(
            reply,
            json!({ "current_song": {"song": { "artist": "artist1", "id": 1, "length": 10, "title": "title1" },
                "user": "alice"},
                "position": 5,
                "queue": [ {"song": { "artist": "artist2", "id": 2, "length": 15, "title": "title2" }, "user": "bob", "eta": 5 } ],
                "state": "Playing" })
        );
    }

    #[test]
    fn history() {
        let test = setup();

        fn check_history(result: Value) {
            let result = result.as_array().unwrap();
            assert_eq!(result.len(), 1);
            let result = &result[0];
            assert_eq!(result["id"], 1);
            assert!(result["played"].is_null());
            assert!(result["queued"].as_str().unwrap().len() > 0);
            assert_eq!(result["user"], "Alice");
            assert_eq!(
                result["song"],
                json!( { "id": 1, "artist": "artist1", "title": "title1", "length": 10 })
            );
        }

        check_history(test.get_json("/api/history"));
        assert_eq!(test.get_json("/api/history/nope"), json!([]));
        check_history(test.get_json("/api/history/Alice"));
    }

    #[test]
    fn all() {
        let test = setup();
        assert_eq!(
            test.get_json("/api/all"),
            json!([ { "id": 1, "artist": "artist1", "title": "title1", "length": 10 } ]),
        );
    }
}

// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use failure::Error;
use gstreamer as gst;

pub fn init() -> Result<(), Error> {
    unsafe {
        x11::xlib::XInitThreads();
    }
    gst::init()?;
    gtk::init()?;

    #[cfg(feature = "cdg-plugin")]
    {
        gstcdg::plugin_register_static()?;
    }

    Ok(())
}

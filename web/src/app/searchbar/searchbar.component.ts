import {Component, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchBarComponent implements OnInit {
  @Output() searchTerm$ = new Subject<string>();

  constructor() {
  }

  ngOnInit() {
  }

  search(terms: string) {
    this.searchTerm$.next(terms.trim());
  }
}

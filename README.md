# Karapulse <a href='https://flathub.org/apps/details/org.karapulse.Karapulse'><img width='100' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>

<div align="center">
  <img src="data/icons/karapulse.svg" width="20%" />
</div>

Karapulse is a Linux karaoke player supporting CDG/MP3 as well as video files. It provides a self-served web application that singers can use with their phone to search for and queue their favorite songs.

## Features

- Support CDG/MP3; automatically match `.mp3` and `.cdg` files if they have the same basename
- Support all video formats.
- Smart songs queueing algorithm ensuring a fair turn distribution among singers.
- Built-in web mobile friendly client usable on desktop as well.
- Full remote control, once setup no need for mouse or keyboard.
- Free Software ([GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)).
- Written in [Rust](https://www.rust-lang.org/) using [GStreamer](https://gstreamer.freedesktop.org/https://gstreamer.freedesktop.org/).

## Setup

- The easiest way to install Karapulse is using [the official Flatpak package from Flathub](https://flathub.org/apps/details/org.karapulse.Karapulse).
- Alternatively see [BUILD.md](BUILD.md) explaining how to build Karapulse from sources.
- First you need to index your songs collection. This can take a while if you have a lot of songs but needs to be done only once.
```
flatpak run --command=karapulse-manage-db org.karapulse.Karapulse add-dir $HOME/karaoke
```
- Then start the `karapulse` binary, you can use the 'f' key to toggle fullscreen mode.
- Connect to the displayed URL using any web browser and use the web interface to search and queue songs.
- Grab some drinks and start singing!

## Hardware

Karapulse currently doesn't have any support for microphone. I'm using [this kit](https://www.amazon.de/gp/product/B07GRLHXGB/) to receive sound from the microphones, mix it with the sound from my laptop and output it to my sound system.

## Screenshots
### Player
![Video playback](data/screenshots/playback-video.png "Video playback")
![CDG playback](data/screenshots/playback-cdg.png "CDG playback")

### Client
![Mobile search](data/screenshots/mobile-search.png "Mobile search")
